import uuid
import ipaddr

from neutronclient.v2_0 import client
from openstack_helpers.common import get_openrc_env


class NeutronHelper(object):
    def __init__(self):
        self.client = client.Client(**get_openrc_env())

    def get_network(self, network_id):
        return NeutronNetwork(self.client.show_network(network_id))

    def get_network_by_name(self, network_name):
        # TODO: See if there is a more efficient way to do this
        nets = self.client.list_networks()
        for net in nets['networks']:
            if net['name'] == network_name:
                return NeutronNetwork({'network': net})

    def create_network(self, name=None):
        data = {
            'network': {}
        }
        if name:
            data['network']['name'] = name
        return NeutronNetwork(json_data=self.client.create_network(data))

    def delete_network(self, network):
        self.client.delete_network(network.id)

    def create_router(self, name, external_net_name=None):
        router = {'router': {'name': name}}
        if external_net_name:
            net = self.get_network_by_name(external_net_name)
            router['router']['external_gateway_info'] = {'network_id': net.id}
        return NeutronRouter(
            json_data=self.client.create_router(router), neutron_helper=self)

    def delete_router(self, router):
        return router.delete()

    def create_subnet(self, name, network_id, cidr, ip_version=4, **kwargs):
        data = {
            'subnet': {
                'name': name,
                'network_id': network_id,
                'cidr': cidr,
                'ip_version': ip_version,
            }
        }
        for k, v in kwargs.iteritems():
            data['subnet'][k] = v

        return NeutronSubnet(json_data=self.client.create_subnet(data))

    def delete_subnet(self, subnet):
        self.client.delete_subnet(subnet.id)


class NeutronObject(object):
    def __init__(self, json_data):
        self.json = json_data

    @property
    def id(self):
        return self.json[self.object_type]['id']

    @property
    def name(self):
        return self.json[self.object_type]['name']


class NeutronNetwork(NeutronObject):
    object_type = 'network'


class NeutronRouter(NeutronObject):
    object_type = 'router'

    def __init__(self, json_data, neutron_helper):
        super(NeutronRouter, self).__init__(json_data)
        self.neutron_helper = neutron_helper

    def delete(self):
        self.neutron_helper.client.delete_router(self.id)

    def add_interface(self, subnet):
        return NeutronRouterInterface(
            self.neutron_helper.client.add_interface_router(
                self.id, {'subnet_id': subnet.id}))

    def remove_interface(self, interface):
        self.neutron_helper.client.remove_interface_router(
            self.id, {'port_id': interface.port_id})

    def get_interfaces(self):
        return self.neutron_helper.client.list_ports(
            device_id=self.id)['ports']


class NeutronSubnet(NeutronObject):
    object_type = 'subnet'

    @property
    def cidr(self):
        return self.json[self.object_type]['cidr']

    @property
    def network_id(self):
        return self.json[self.object_type]['network_id']

    def get_network(self):
        '''
        Get the network which this subnet belongs to
        '''
        return NeutronHelper().get_network(self.network_id)


class NeutronRouterInterface(object):
    def __init__(self, json_data):
        self.json = json_data

    @property
    def id(self):
        return self.json['id']

    @property
    def port_id(self):
        return self.json['port_id']


def network_generator(router, network_cidr, subnet_prefix_len=24, **kwargs):
    '''
    Generator which will yield virtual networks. This is a high level function
    which will do the following:

        1) Create a neutron network
        2) Create a subnet on the network
        3) Attach the network to the provided router

    :param router: The neutron router to create the networks on
    :param str network_cidr: The network from which subnets will be generated
    :param int subnet_prefix_len: CIDR style subnet size for created subnets

    Optional kwargs:

    :param list dns_nameservers: List of IP addresses to use for DNS servers
    '''
    helper = NeutronHelper()
    parent_net = ipaddr.IPNetwork(network_cidr)
    subnet_generator = parent_net.iter_subnets(new_prefix=subnet_prefix_len)
    for subnet_addr in subnet_generator:
        # Create a network
        network = helper.create_network(name=get_uuid())
        # Create a subnet
        start_addr, end_addr = first_and_last_addr(subnet_addr, padding=5)
        subnet = helper.create_subnet(
            name=get_uuid(),
            network_id=network.id,
            cidr=str(subnet_addr),
            allocation_pools=[
                {'start': str(start_addr), 'end': str(end_addr)}],
            **kwargs)
        # Attach the network to the router
        router.add_interface(subnet)
        yield subnet


def get_uuid():
    return str(uuid.uuid4())


def first_and_last_addr(network, padding=0):
    '''
    Return a tuple of (first, last) addresses from provided
    ipaddr.IPNetwork object.

    :param int padding: Leave this many addresses spare at each end of the pool
    '''
    if padding > (network.numhosts / 2) - 2:
        raise ValueError(
            'Padding of {!r} is too great for /{!r} subnet!'.format(
                padding, network.prefixlen))
    hosts = list(network.iterhosts())
    first = hosts[padding]
    last = hosts[-padding] if padding > 0 else hosts[-1]
    return first, last
