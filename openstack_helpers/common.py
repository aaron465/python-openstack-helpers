from os import environ


def get_openrc_env(fail_hard=True, python_names=True):
    '''
    Loads openrc style environment variables and returns them in a format which
    will work for passing directly into the __init__ functions of the other
    python openstack libraries.

    For example:

        from neutronclient.v2_0 import client
        neutron_client = client.Client(**get_openrc_env())

    If you pass fail_hard=False then instead of raising KeyError when an
    environment variable is missing, it will just have a value of None in the
    resulting dictionary.

    If you pass python_names=False then you will get the standard openrc
    variable names (OS_USERNAME etc) instead of the python-openstack style.
    '''
    if fail_hard:
        getter_func = environ.__getitem__
    else:
        getter_func = environ.get

    if python_names:
        return {
            'username': getter_func('OS_USERNAME'),
            'password': getter_func('OS_PASSWORD'),
            'tenant_name': getter_func('OS_TENANT_NAME'),
            'auth_url': getter_func('OS_AUTH_URL'),
        }
    else:
        return {
            'OS_USERNAME': getter_func('OS_USERNAME'),
            'OS_PASSWORD': getter_func('OS_PASSWORD'),
            'OS_TENANT_NAME': getter_func('OS_TENANT_NAME'),
            'OS_AUTH_URL': getter_func('OS_AUTH_URL'),
        }
