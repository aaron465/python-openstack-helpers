from setuptools import setup

setup(
    name='openstack_helpers',
    version='1',
    py_modules=['openstack_helpers'],
    install_requires=[
        'python-neutronclient',
        'ipaddr',
    ],
)
